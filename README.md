# public domain esperanto art

This is art that I've created that is related to Esperanto in some way.
I am releasing it into the public domain for use anywhere.

## Who am I?

I'm Rachel Wil Sha Singh, Rachel Jane Morris (before marriage :)
aka *rejcx*, *RWSS*, or *Moosie*.
I"ve ran several conlang websites over the years, such as
Esperanimeo and Áya Dan. I began learning Esperanto in 2011.
I run the Kanhttps://gitlab.com/rachels-conlangs/public-domain-esperanto-artsas City Esperanto group as well.

You can contact me at *Rachel@Moosader.com*

## Where can you find me?

* [Moosadee.com](http://www.moosadee.com)
* [Rachel.LikesPizza.com](http://rachel.likespizza.com)
* [Rachel's Conlangs @ YouTube](https://www.youtube.com/rachelsconlangs)
* [rejcx @ DeviantArt](https://www.deviantart.com/rejcx)
* [moosader @ DeviantArt](https://www.deviantart.com/moosader)

## View of the art

You can also download the art by clicking the image name in the
directory listing.

![basic_esperanto_words.png](basic_esperanto_words.png)

![esperanto_girl.png](esperanto_girl.png)

![esperanto_girl_by_rejcx_dbux8o0.png](esperanto_girl_by_rejcx_dbux8o0.png)

![esperanto_valentine_s_day_cards.png](esperanto_valentine_s_day_cards.png)

![z-emoji1.png](z-emoji1.png)
![z-emoji2.png](z-emoji2.png)
![z-emoji3.png](z-emoji3.png)
![z-emoji4.png](z-emoji4.png)
![z-emoji5.png](z-emoji5.png)
